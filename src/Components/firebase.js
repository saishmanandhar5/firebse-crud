import firebase from "firebase";


const firebaseConfig = {
    apiKey: "AIzaSyCTDhPfQpjMSYGJlICry0y7x7S1CuiTC7M",
    authDomain: "to-do-list-ef739.firebaseapp.com",
    databaseURL: "https://to-do-list-ef739-default-rtdb.firebaseio.com",
    projectId: "to-do-list-ef739",
    storageBucket: "to-do-list-ef739.appspot.com",
    messagingSenderId: "353555145131",
    appId: "1:353555145131:web:414514ef5a69d7f79532ee"
  };

  firebase.initializeApp(firebaseConfig);
  const database = firebase.database();
  // const database = getFirestore(app);

  export default database;
  