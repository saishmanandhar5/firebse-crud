import axios from 'axios';
import React, {  useEffect, useState } from 'react'
import database from './firebase';

export default function Todolist() {
    const editButton=()=>{
        
     }
    const [task,setTask]= useState([
    {
        id:1,
       title:''
    }
    ]);
    const [title,setTitle]= useState('');
    useEffect(  ()=>{
      console.log('use effect')
      getData();
    },[]);

    const getData= async()=>{
      await database.ref("To-Do").get().
      then((res)=>{
        
         const val =Object.values(res.val())
         console.log(val)
          setTask(val);
       
      })
      
    }
   
    
    const  postTask = async (e)=>{
        e.preventDefault();
        setTask(()=>[...task, title])
          // push();
          await database.ref("To-Do").push({
            
            title:title})
          .catch(alert);

          getData()
        
    }
        
       
  return (
   <>
   <div className="todolist" style={{display:'flex', justifyContent:'center', flexDirection:'column', alignItems:'center', marginTop:'5%'}}>
    <div style={{padding:'10px'}}>

    <label style={{fontSize:'30px'}}>To Do List</label>
    </div>
    <div>

    <input style={{fontSize:'20px'}} type="text" onChange={(e)=>setTitle(e.target.value)} placeholder='Tasks to do...' />
    <input style={{fontSize:'20px',marginLeft:'5px'}} onClick={postTask} type="button" value="Add Task" />
    </div>
   <div style={{fontSize:'20px', paddingTop:'20px'}}>
    List
   </div>
   </div>
   <div className="display" style={{fontSize:'20px', paddingTop:'20px',display:'flex', justifyContent:'center',flexDirection:'column', alignItems:'center',}}>
    {
        task?.map((value,idx)=><form method="post" key={idx}>
         
            <input type="text" value={value.title} disabled style={{textAlign:'center', fontSize:'20px',}}/> 
           <span style={{padding:'0 10px'}}><button onClick={editButton} style={{padding:'0 5px'}}>Edit</button></span>    
           <span ><button style={{padding:'0 5px'}}>Delete</button></span>
            </form>)

    }
   </div>
   </>
  )
}
